#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class Node:
    def __init__(self, value):
        self.value = value
        self.next = None
    
    def show_node(self):
        print(self.value)


class LinkedList:
    def __init__(self):
        self.first = None

    def show(self):
        if self.first.next == None:
            print('Empty list')
            return None

        current = self.first
        while current != None:
            current.show_node()
            current = current.next
            
    def insert(self, value):
        new_node = Node(value)
        new_node.next = self.first
        self.first = new_node
    
    def search(self, value):
        if self.first.next == None:
            print('Empty list')
            return None
        
        current = self.first
        while current.value != value:
            if current.next == None:
                return None
            else:
                current = current.next
        
        return current


            
    def delete(self):
        if self.first.next == None:
            print('Empty list')
            return None
        
        temp = self.first
        self.first = self.first.next
        return temp


if __name__ == '__main__':
    linked_list = LinkedList()

    linked_list.insert(1)
    linked_list.insert(10)
    linked_list.insert(20)
    linked_list.insert(5)
    linked_list.insert(15)
    linked_list.show()
    print('\nRemove...\n')
    linked_list.delete()
    linked_list.show()
    print('\nSearch...\n')
    value_search = linked_list.search(10)
    if value_search != None:
        print('Found: ', value_search.value)
    else:
        print('Not Found.')