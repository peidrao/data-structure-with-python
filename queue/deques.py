#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class Deque:
    def __init__(self, capacity):
        self.capacity = capacity
        self.first = -1
        self.end = 0
        self.elements = 0
        self.values = np.empty(self.capacity, dtype=int)

    def __is_full(self):
        return (self.first == 0 and self.end == self.capacity -1) or (self.first == self.end + 1)
    
    def __is_empty(self):
        return self.first == -1

    def insert_begin(self, value):
        if self.__is_full():
            print('full deque')
            return 
        if self.first == -1:
            self.first = 0
            self.end = 0
        elif self.first == 0:
            self.first = self.capacity -1 
        else:
            self.first -= 1
        self.values[self.first] = value
    
    def insert_end(self, value):
        if self.__is_full():
            print('full deque')
            return 
        if self.first == -1:
            self.first = 0
            self.end = 0
        elif self.end == self.capacity -1:
            self.end = 0
        else:
            self.end += 1
        self.values[self.end] = value

    def remove_begin(self):
        if self.__is_empty():
            print('deck is empty')

        if self.first == self.end:
            self.first = -1
            self.end = -1
        else:
            if self.first == self.capacity -1:
                self.first = 0
            else:
                self.first += 1
    
    def remove_end(self):
        if self.__is_empty():
            print('deck is empty')

        if self.first == self.end:
            self.first = -1
            self.end = -1
        elif self.first == 0:
            self.end = self.capacity - 1
        else: 
            self.end = -1 

    def get_begin(self):
        if self.__is_empty():
            print('deque is empty')
            return
    
        return self.values[self.first]

    def get_end(self):
        if self.__is_empty() or self.end < 0:
            print('deck is empty')
            return
        return self.values[self.end]

if __name__ == '__main__':
    deque = Deque(5)
    deque.insert_end(5)
    print(deque.get_begin(), deque.get_end())
    deque.insert_end(10)
    print(deque.get_begin(), deque.get_end())
    deque.insert_begin(3)
    print(deque.get_begin(), deque.get_end())
    deque.insert_begin(2)
    deque.insert_end(11)
    print(deque.get_begin(), deque.get_end())
    deque.remove_begin()
    deque.remove_end()
    print(deque.get_begin(), deque.get_end())
