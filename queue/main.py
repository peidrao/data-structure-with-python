#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class CircularQueue:
    def __init__(self, capacity):
        self.capacity = capacity
        self.first = 0
        self.end = -1
        self.elements = 0
        self.values = np.empty(self.capacity, dtype=int)

    def __is_full(self):
        return self.elements == self.capacity
    
    def __is_empty(self):
        return self.elements == 0
    
    def enqueue(self, value):
        if self.__is_full():
            print('Full queue')
            return 
        if self.end == self.capacity -1:
            self.end -= 1
        self.end += 1
        self.values[self.end] = value
        self.elements += 1

    def dequeue(self):
        if self.__is_empty():
            print('Queue is empty')
            return 
        temp = self.values[self.first]
        self.first += 1
        if self.first == self.capacity:
            self.first = 0
        self.elements -= 1
        return temp
    
    def first_element(self):
        if self.__is_empty():
            return -1
        return self.values[self.first]


if __name__ == '__main__':
    queue = CircularQueue(5)
    queue.enqueue(5)
    queue.enqueue(4)
    queue.enqueue(10)
    queue.enqueue(55)
    queue.enqueue(12)
    queue.enqueue(1)
    print(queue.values)
    queue.dequeue()
    print(queue.values)
    print(queue.first_element())