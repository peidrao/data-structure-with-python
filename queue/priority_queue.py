#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class PriorityQueue:
    def __init__(self, capacity):
        self.capacity = capacity
        self.elements = 0
        self.values = np.empty(self.capacity, dtype=int)

    def __is_full(self):
        return self.elements == self.capacity
    
    def __is_empty(self):
        return self.elements == 0

    def first_element(self):
        if self.__is_empty():
            return -1
        return self.values[self.elements - 1]

    def enqueue(self, value):
        if self.__is_full():
            print('Full queue')
            return 
        
        if self.elements == 0:
            self.values[self.elements] = value
            self.elements += 1
        else:
            x = self.elements - 1
            while x >= 0:
                if value > self.values[x]:
                    self.values[x+1] = self.values[x]
                else:
                    break
                x -= 1
            self.values[x + 1] = value
            self.elements += 1

    def dequeue(self):
        if self.__is_empty():
            print('Queue is empty')
            return 
        temp = self.values[self.elements - 1]
        self.elements -= 1
        return temp


if __name__ == '__main__':
    queue = PriorityQueue(5)
    queue.enqueue(5)
    queue.enqueue(7)
    queue.enqueue(3)
    queue.enqueue(10)
    queue.enqueue(1)
    print(queue.values)
    print(queue.dequeue())
    print(queue.first_element())
    print(queue.values)
    # print(queue.values)