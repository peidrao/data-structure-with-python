#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class Stack:
    def __init__(self, capacity):
        self.__capacity = capacity
        self.__top = -1
        self.__values = np.empty(self.__capacity, dtype=int)

    def __is_full(self):
        if self.__top == self.__capacity - 1:
            return True
        else:
            return False
    
    def __is_empty(self):
        if self.__top == -1:
            return True
        else:
            return False

    def stack_up(self, value):
        if self.__is_full():
            print('stack is full')
        else:

            self.__top += 1
            self.__values[self.__top] = value

    def unstack(self):
        if self.__is_empty():
            print('Stack is empty')
        else:
            self.__top -= 1

    def get_top(self):
        if self.__top != -1:
            return self.__values[self.__top]
        else:
            return -1


if __name__ == '__main__':
    stack = Stack(5)
    stack.stack_up(5)
    stack.stack_up(10)
    stack.stack_up(15)
    stack.stack_up(20)
    stack.stack_up(1)
    print('Top: ', stack.get_top())
    stack.unstack()
    stack.unstack()
    print('Top: ', stack.get_top())