#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class VectorOrdered:
    def __init__(self, capacity):
        self.capacity = capacity
        self.last_index = -1
        self.values = np.empty(self.capacity, dtype=int)

    def print_list(self):
        if self.last_index == -1:
            print('The vector is empty')
        else:
            for i in range(self.last_index + 1):
                print(i, ' - ', self.values[i])
    
    # O(n)
    def insert(self, value):
        if self.last_index == self.capacity - 1:
            print("Maximum capacity")
            return 
        
        index = 0
        for i in range(self.last_index + 1):
            index = i
            if self.values[i] > value:
                break
            if i == self.last_index:
                index = i + 1

        x = self.last_index
        while x >= index:
            self.values[x+1] = self.values[x]
            x -= 1

        self.values[index] = value
        self.last_index += 1 
    
    def search(self, value):
        for i in range(self.last_index + 1):
            if self.values[i] > value:
                return -1
            if self.values[i] == value:
                return i
            if i == self.last_index:
                return -1
            
    def delete(self, value):
        index = self.search(value)
        if index == -1:
            return -1
        else:
            for i in range(index, self.last_index):
                self.values[i] = self.values[i + 1]
            self.last_index -= 1

    def binary_search(self, value):
        low_index = 0
        high_index = self.last_index

        while True:
            index = int((low_index + high_index) / 2)
            if self.values[index] == value: # Se o valor estiver no meio
                return index
            elif low_index > high_index: # Caso não seja encontrado
                return -1
            else: # Divide os limites
                if self.values[index] < value:
                    low_index = index + 1
                else:
                    high_index = index - 1




if __name__ == '__main__':
    vector = VectorOrdered(10)
    vector.insert(1)    
    vector.insert(5)
    vector.insert(3)
    vector.insert(2)
    vector.insert(10)
    vector.insert(7)
    vector.insert(4)
    vector.insert(11)
    
    print('Busca Linear: ', vector.search(3))
    vector.delete(5)
    # vector.delete(2)
    
    vector.print_list()

    print('Busca Binária: ', vector.binary_search(4))