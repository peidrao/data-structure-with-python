#!/usr/bin/python
# -*- coding: latin-1 -*-

# O(n)
def soma1(n):
    soma = 0
    for i in range(n + 1):
        soma += i
    return soma


# O(3)
def soma2(n):
    return (n * (n+1)) / 2


def aula1():
    print(soma1(10))
    print(soma2(10))


def lista1():
    lista = []
    for i in range(1000):
        lista += [i]
    return lista


def lista2():
    return range(1000)


def aula2():
    lista1() # O(n)



if __name__ == '__main__':
    aula1()
