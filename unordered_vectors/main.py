#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np


class VectorUnordered:
    def __init__(self, capacity):
        self.capacity = capacity
        self.last_index = -1
        self.values = np.empty(self.capacity, dtype=int)

    def print_list(self):
        if self.last_index == -1:
            print('The vector is empty')
        else:
            for i in range(self.last_index + 1):
                print(i, ' - ', self.values[i])
    
    def insert(self, value):
        if self.last_index == self.capacity - 1:
            print("Maximum capacity")
        else:
            self.last_index += 1
            self.values[self.last_index] = value
    
    def search(self, value):
        for i in range(self.last_index + 1):
            if value == self.values[i]:
                return i
        return -1
    
    def delete(self, value):
        index = self.search(value)
        if index == -1:
            return -1
        else:
            for i in range(index, self.last_index):
                self.values[i] = self.values[i + 1]
            self.last_index -= 1




if __name__ == '__main__':
    vector = VectorUnordered(5)
    vector.insert(1)    
    vector.insert(3)
    vector.insert(5)
    vector.print_list()
    print(vector.search(2))
    print(vector.search(5))

    vector.delete(3)
    vector.print_list()

